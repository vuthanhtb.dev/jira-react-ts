import React from 'react';
import './App.css';
import { useAuth } from './context/auth-context';
import { AuthenticatedApp } from './authenticated-app';
import { Auth } from './pages/auth';

function App() {
  const { user } = useAuth();

  return <div className='App'>{user ? <AuthenticatedApp /> : <Auth />}</div>;
}

export default App;
