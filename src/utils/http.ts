import { stringify } from 'qs';
import * as auth from 'auth-provider';
import { useAuth } from '../context/auth-context';

const apiUrl = 'http://localhost:3001';

interface Config extends RequestInit {
  token?: string;
  data?: object;
}

export const http = async (
  endpoint: string,
  { data, token, headers, ...customConfig }: Config = {}
) => {
  const config = {
    method: 'GET',
    headers: {
      Authorization: token ? `Bearer ${token}` : '',
      'Content-Type': 'application/json',
    },
    ...customConfig,
  };

  if ('GET' === config.method.toUpperCase()) {
    endpoint += `?${stringify(data)}`;
  } else {
    config.body = JSON.stringify(data || {});
  }

  return window.fetch(`${apiUrl}/${endpoint}`, config).then(async (resp: Response) => {
    if (401 === resp.status) {
      await auth.logout();
      window.location.reload();
      return Promise.reject({ message: 'un_authorization' });
    }

    if (resp.ok) {
      return await resp.json();
    }

    return Promise.reject(data);
  });
};

export const useHttp = () => {
  const { user } = useAuth();
  return (...[endpoint, config]: Parameters<typeof http>) =>
    http(endpoint, { ...config, token: user?.token });
};
