import { useEffect, useState } from 'react';

export const useMount = (callback: () => void) => {
  useEffect(() => {
    callback();
  }, []);
};

export const useDebounce = <V>(value: V, delay?: number) => {
  const [debouncedValue, setDebouncedValue] = useState(value);

  useEffect(() => {
    // Mỗi lần sau khi giá trị thay đổi, đặt hẹn giờ
    const timer = setTimeout(() => setDebouncedValue(value), delay);
    // Chạy mỗi lần sau khi useEffect cuối cùng được xử lý
    return () => {
      clearTimeout(timer);
    };
  }, [value, delay]);
  return debouncedValue;
};

export const getParams = (url: any) => {
  let params: any = {};
  let _url = url || window.location.href;
  if (window.URLSearchParams) {
    new URL(_url).searchParams.forEach(
      (value: string, key: string) => (params[key] = decodeURIComponent(value))
    );
  } else {
    _url.replace(/[#|?&]+([^=#|&]+)=([^#|&]*)/gi, function (m: any, key: string, value: string) {
      params[key] = decodeURIComponent(value);
    });
  }
  return params;
};

export const isFalsy = (val: unknown) => (val === 0 ? false : !val);

export const cleanObject = (obj: object) => {
  const result = { ...obj };
  Object.keys(obj).forEach((key: string) => {
    // @ts-ignore
    const value = obj[key];
    if (isFalsy(value)) {
      // @ts-ignore
      delete result[key];
    }
  });
  return result;
};

export const useArray = <T>(initialArray: T[]) => {
  const [value, setValue] = useState(initialArray);

  return {
    value,
    setValue,
    add: (item: T) => setValue([...value, item]),
    clear: () => setValue([]),
    removeIndex: (index: number) => {
      const copy = [...value];
      copy.splice(index, 1);
      setValue(copy);
    },
  };
};
