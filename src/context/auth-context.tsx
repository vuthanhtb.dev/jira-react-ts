import React, { ReactNode, useState } from 'react';
import * as auth from 'auth-provider';
import User from 'model/user';
import { http } from 'utils/http';
import { useMount } from '../utils';

interface AuthForm {
  username: string;
  password: string;
}

const bootstrapUser = async () => {
  const token = auth.getToken();

  if (token) {
    const data = await http('me', { token });
    return data.user;
  }

  return null;
};

const AuthContext = React.createContext<
  | {
      user: User | null;
      login: (form: AuthForm) => Promise<void>;
      register: (form: AuthForm) => Promise<void>;
      logout: () => Promise<void>;
    }
  | undefined
>(undefined);
AuthContext.displayName = 'AuthContext';

export const AuthProvider = ({ children }: { children: ReactNode }) => {
  const [user, setUser] = useState<User | null>(null);

  const login = (form: AuthForm) => auth.login(form).then(setUser);
  const register = (form: AuthForm) => auth.register(form).then(setUser);
  const logout = () => auth.logout().then(() => setUser(null));

  useMount(() => {
    bootstrapUser().then(setUser);
  });

  return (
    <AuthContext.Provider value={{ user, login, register, logout }}>
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => {
  const context = React.useContext(AuthContext);

  if (!context) {
    throw new Error('useAuth error');
  }

  return context;
};
