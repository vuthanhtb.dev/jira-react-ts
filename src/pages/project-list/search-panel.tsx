import React from 'react';
import User from '../../model/user';

interface IProps {
  users: User[];
  params: {
    name: string;
    personId: string;
  };
  setParams: (param: IProps['params']) => void;
}

const SearchPanel = ({ params, setParams, users }: IProps) => {
  return (
    <form action=''>
      <input
        type='text'
        value={params.name}
        onChange={event =>
          setParams({
            ...params,
            name: event.target.value,
          })
        }
      />
      <select
        value={params.personId}
        onChange={event =>
          setParams({
            ...params,
            personId: event.target.value,
          })
        }
      >
        <option value=''>Chọn...</option>
        {users.map((user: User) => (
          <option key={user.id} value={user.id}>
            {user.name}
          </option>
        ))}
      </select>
    </form>
  );
};

export default SearchPanel;
