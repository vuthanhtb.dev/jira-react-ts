import React, { useEffect, useState } from 'react';
import SearchPanel from './search-panel';
import ListComponent from './list';
import User from 'model/user';
import Project from 'model/project';
import { cleanObject, useDebounce, useMount } from 'utils';
import { useHttp } from 'utils/http';

function ProjectListPage() {
  const [params, setParams] = useState({
    name: '',
    personId: '',
  });
  const debounceParam = useDebounce(params, 200);

  const [users, setUsers] = useState<User[]>([]);
  const [list, setList] = useState<Project[]>([]);

  const client = useHttp();

  useEffect(() => {
    client('projects', { data: cleanObject(debounceParam) }).then(setList);
  }, [debounceParam]);

  useMount(() => {
    client('users').then(setUsers);
  });

  return (
    <div>
      <SearchPanel params={params} setParams={setParams} users={users} />
      <ListComponent list={list} users={users} />
    </div>
  );
}

export default ProjectListPage;
