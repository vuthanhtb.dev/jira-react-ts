import React from 'react';
import Project from '../../model/project';
import User from '../../model/user';

interface IProps {
  list: Project[];
  users: User[];
}

const ListComponent = ({ list, users }: IProps) => {
  return (
    <div>
      <table>
        <tr>
          <th>Name</th>
          <th>Principal</th>
        </tr>
        <tbody>
          {list.map((project: Project) => (
            <tr key={project.id}>
              <td>{project.name}</td>
              <td>{users.find((user: User) => user.id === project.personId)?.name}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default ListComponent;
