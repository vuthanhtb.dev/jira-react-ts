import React, { FormEvent } from 'react';
import { useAuth } from 'context/auth-context';

export const Login = () => {
  const { login, user } = useAuth();

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const username = (e.currentTarget.elements[0] as HTMLInputElement).value;
    const password = (e.currentTarget.elements[1] as HTMLInputElement).value;
    login({ username, password });
  };

  return (
    <form onSubmit={handleSubmit}>
      {user && <div>{user.token}</div>}
      <div>
        <label htmlFor='username'>Tài khoản</label>
        <input type='text' id='username' />
      </div>
      <div>
        <label htmlFor='password'>Mật khẩu</label>
        <input type='password' id='password' />
      </div>
      <button type='submit'>Đăng nhập</button>
    </form>
  );
};
