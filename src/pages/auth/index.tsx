import { useState } from 'react';
import { Register } from './register';
import { Login } from './login';

export const Auth = () => {
  const [isRegister, setRegister] = useState<boolean>(false);

  return (
    <div>
      {isRegister ? <Register /> : <Login />}
      <button onClick={() => setRegister(!isRegister)}>{isRegister ? 'Login' : 'Register'}</button>
    </div>
  );
};
