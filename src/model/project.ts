interface Project {
  id: string;
  name: string;
  personId: string;
  pin: boolean;
  organization: string;
}

export default Project;
