module.exports = (req, res, next) => {
  if (req.method === 'POST' && req.path === '/login') {
    if (req.body.username === 'admin' && req.body.password === '123456') {
      return res.status(200).json({
        user: {
          id: Math.floor(Math.random() * 100 + 1),
          name: 'Jira',
          token: Math.floor(Math.random() * 1000000000000 + 1),
        },
      });
    } else {
      return res.status(400).json({
        message: 'Đăng nhập không thành công.',
      });
    }
  }
  next();
};
