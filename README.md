## `PRETTIER`

```
yarn add --dev --exact prettier
```

```
echo {}> .prettierrc.json
```

```
touch .prettierignore
```

```
npx mrm lint-staged
```

```
yarn add eslint-config-prettier -D
```

```
yarn add @commitlint/{config-conventional,cli} -D
```

```
echo "module.exports = {extends: ['@commitlint/{config-conventional,cli}']}" > commitlint.config.js
```

### `MOCK JSON SERVER`

```
yarn add json-server -D
```
